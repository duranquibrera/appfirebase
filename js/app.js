// Import the functions you need from the SDKs you need
import { initializeApp } from
"https://www.gstatic.com/firebasejs/10.11.1/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
import { getDatabase,onValue,ref as refS,set,get, update, child,remove}
from "https://www.gstatic.com/firebasejs/10.11.1/firebase-database.js"
//firebase.google.com/f irebasejs/10.11.1/firebase-database.js";
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBK0nHrSlFrniSyijCZcXLjT2vniDS3-t0",
    authDomain: "webcrud-b3ccb.firebaseapp.com",
    databaseURL: "https://webcrud-b3ccb-default-rtdb.firebaseio.com",
    projectId: "webcrud-b3ccb",
    storageBucket: "webcrud-b3ccb.appspot.com",
    messagingSenderId: "825180979694",
    appId: "1:825180979694:web:7ff060c0b1f450d8254eff"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
// declarar unas variables global
var numSerie =0;
var marca ="";
var modelo = "";
var descripcion = "";
var urlImag = "";
// funciones
Listarproductos();

function leerInputs(){
    numSerie = document.getElementById('txtNumSerie').value;
    marca = document.getElementById('txtMarca').value;
    modelo = document.getElementById('txtModelo').value;
    descripcion = document.getElementById('txtDescripcion').value;
    urlImag = document.getElementById('txtUrl').value;
    Listarproductos();
}

function mostrarMensaje(mensaje){
    var mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent= mensaje;
    mensajeElement.style.display= 'block';
    setTimeout(()=>{
        mensajeElement.style.display = 'none';
    },1000)
}

// agregar producto a la base de datos
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto() {
    leerInputs();
    if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
        mostrarMensaje("Faltaron datos por capturar");
        return;
    }
    set(refS(db, 'Automoviles/' + numSerie), {
        numSerie: numSerie,
        marca: marca,
        modelo: modelo,
        descripcion: descripcion,
        urlImag: urlImag
    }).then(() => {
        alert("Se agregó con éxito");
        Listarproductos();
    }).catch((error) => {
        alert("Ocurrió un error");
    });
}

function Listarproductos() {
    const dbRef = refS(db, 'Automoviles');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;

            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);

            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.marca;
            fila.appendChild(celdaNombre);

            var celdaPrecio = document.createElement('td');
            celdaPrecio.textContent = data.modelo;
            fila.appendChild(celdaPrecio);

            var celdaCantidad = document.createElement('td');
            celdaCantidad.textContent = data.descripcion;
            fila.appendChild(celdaCantidad);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImg;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    }, { onlyOnce: true });
}


function buscarAutomovile (){
    numSerie = document.getElementById('txtNumSerie').value.trim();
    if (numSerie === ""){
        mostrarMensaje("Falto capturar el numero de serie");
        return;
    }

    const dbref = refS(db);
    get(child(dbref,'Automoviles/' + numSerie) 
    //<- Se agrego el ' /' al final deautomiviles
    ).then( (snapshot)=>{
    if(snapshot.exists()){
        marca= snapshot.val().marca;
        modelo = snapshot.val().modelo;
        descripcion = snapshot.val().descripcion;
        urlImag = snapshot.val().urlImag;
        escribirInputs();
    }
    else {
        limpiarInputs();
        mostrarMensaje(" No se encontro el registro");

    }
    });
}

// Se agrego la funcion actualizar
function actualizarAutomovil() {
    leerInputs();
    if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
        mostrarMensaje("Favor de capturar toda la información.");
        return;
    }
    alert("actualizar");
    update(refS(db, 'Automoviles/' + numSerie), {
        numSerie:numSerie,
        marca: marca,
        modelo: modelo,
        descripcion: descripcion,
        urlImg: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizó con éxito.");
        limpiarInputs();
        Listarproductos();
    }).catch((error) => {
        mostrarMensaje("Ocurrió un error: " + error);
    });
    Listarproductos();
}
// Se agrego la funcion elimina eliminar Automovil
function eliminarAutomovil() {
    let numSerie= document.getElementById('txtNumSerie').value.trim();
    if (numSerie === "") {
        mostrarMensaje("No se ingresó un Codigo válido.");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {

    if (snapshot.exists()) {
        remove(refS(db, 'Automoviles/' + numSerie))
        .then(() => {
            mostrarMensaje("Producto eliminado con éxito.");
            limpiarInputs();
            Listarproductos();
        })
        .catch((error) => {
            mostrarMensaje("Ocurrió un error al eliminar el producto: " + error);
        });
    } else {
    limpiarInputs();
    mostrarMensaje("El producto con ID " + numSerie + " no existe.");
    }
    });
    Listarproductos(); 
}
function escribirInputs(){
    document.getElementById('txtMarca').value= marca;
    document.getElementById('txtModelo').value= modelo;
    document.getElementById('txtDescripcion').value= descripcion;
    document.getElementById('txtUrl').value= urlImag;
}
function limpiarInputs(){
    document.getElementById('txtNumSerie').value= '';
    document.getElementById('txtMarca').value= '';
    document.getElementById('txtModelo').value= '';
    document.getElementById('txtDescripcion').value= ''; 
    // se cambio a txtDescripcion con D mayuscula
    document.getElementById('txtUrl').value='';

}

// se codificaron los metodos
const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click',buscarAutomovile);
const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click',eliminarAutomovil);
const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarAutomovil);